# ForceMajeure
---
### DevDependencies
- gulp
- gulp-autoprefixer
- gulp-postcss
- gulp-sass
- gulp-terser
- nodemon
<br>
### Marche à suivre pour starter le serveur web:

1 - Pour start gulp dans un terminal
```node.js
gulp
```
2 - Pour starter le serveur express dans un second terminal
```node.js
npm start
```
* Une fois les terminaux en fonction, pour arrêter l'exécution:
1- Sélectionner le terminal
2- appuyez sur les touche CTRL + C ou
⌘ + C

---
<br>
##### Rappel
* Pour programmer le script des pages .hbs veuillez utiliser le ficher script.js
(Il se trouve dans le fichier app -> js).
<br>
* Il en est de même pour le css des pages .hbs
(Il se trouve dans le ficher app -> scss)
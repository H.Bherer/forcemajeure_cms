// hide navbar

var scroll1 = window.pageYOffset;
window.onscroll = function(){
    var scroll2 = window.pageYOffset;
    if (scroll1 > scroll2) {
        document.getElementById('header').style.top = "0";

    } else {
        document.getElementById('header').style.top = "-88px";
    }
    scroll1 = scroll2;
}

const vw = (coef) => window.innerWidth * (coef/100)
var speed = 80;

var animTypewriter = $('.O').attr('placeholder').split('');

$('.O').attr('placeholder','')
curIndex = 0;



var tl = gsap.timeline({
    onComplete:addToPlaceholder
});

tl.delay(0.5)

tl.from(".TopTitle", {
    duration: 1,
    ease: "back.inOut(0.8)",
    x:"-2000",
})

tl.from(".BottomTitle", {
    duration: 1,
    x:'2000',
    ease: "back.inOut(0.8)",
}, '-=0.4')

tl.to(".O", {
    duration: 0.7,
    width: vw(21.1),
    ease: "back.out(0.8)",

}, '+=0.3')

tl.to(".ring", {

    backgroundColor: "rgba(20,20,20)",
    ease: "ease.out(0.5)",

})


tl.to(".O", {

    border: 1,
    ease: "ease.out(0.8)",

}, '-=0.4')

tl.to(".ring", {

    backgroundColor: "rgba(29,29,29)",
    ease: "back.out(0.2)",

}, '-=0.3')





tl.to("#heroCTA", {
    opacity:0,
}, '-=1')
tl.from("#heroCTA", {
    duration: 0.5,
    ease: "back.inOut(0.3)",
    y:"800",
    opacity:100,
}, '-=0.3')



/* -------------------------------------------------*/

function addToPlaceholder(){
    var curVal = $(".O").attr("placeholder");
    $(".O").attr("placeholder", curVal + animTypewriter[curIndex]  );
    curIndex++;
    if( curIndex < animTypewriter.length ){
      setTimeout( addToPlaceholder , speed );
    }
  }
  

const { src, dest, watch, series } = require('gulp');
const sass = require('gulp-sass');
const postcss = require('gulp-postcss');
const terser = require('gulp-terser');
const gulpautoprefixer = require('gulp-autoprefixer');

function jsTask() {
    return src('./app/js/script.js', { sourcemaps: true })
        //.pipe(terser()) disabled for coding
        .pipe(dest('./public/javascripts', { sourcemaps: '.' }));
}
function scssTask() {
    return src('./app/scss/style.scss', { sourcemaps: true })
        .pipe(sass())
        .pipe(dest('./public/stylesheets', { sourcemaps: '.' }));
}
function watchTask(){
    watch(['app/scss/**/*.scss', 'app/js/**/*.js'], series(scssTask, jsTask,));
  }

exports.default = series(
    scssTask,
    jsTask,
    watchTask
  );
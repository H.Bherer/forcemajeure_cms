const Mongoose = require("mongoose");
let Schema = Mongoose.Schema;

const UserSchema = new Schema({
    username: {
        type: String,
        required: true,
        unique:true
    }, 
    password:{
        type:String
    }
});

exports.UserModel = Mongoose.model("User", UserSchema);
var express = require('express');
var router = express.Router();
var apiController = require('../controllers/apiController');

// API GET article on blog page
router.get('/', apiController.getAllArticle);
router.get('/actualites', apiController.getAPIByActualites);
router.get('/conseils', apiController.getAPIByConseils);
router.get('/referencement', apiController.getAPIByReferencement);
router.get('/analytics', apiController.getAPIByAnalytics);
router.get('/web', apiController.getAPIByWeb);
router.get('/:id', apiController.getArticleById);

module.exports = router;
